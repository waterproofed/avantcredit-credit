
# line of credit

this is the solution to the line of credit programming challenge

## installation

```bash

    git clone git@bitbucket.org:waterproofed/avantcredit-credit.git

    cd avantcredit-credit

    bundle install

```

## tests

```bash

    rake

```

## notes

this was developed under the default ruby installation of OSX

    ruby 2.0.0p481 (2014-05-08 revision 45883) [universal.x86_64-darwin14]

## remarks

this programming challenge was significantly easier to complete (as compared
to the factors and caching problem) as the requirements were remarkably
clear and concise.

