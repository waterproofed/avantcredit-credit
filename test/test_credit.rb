require 'minitest/autorun'
require 'date'
require_relative '../lib/credit'

class TestCredit < MiniTest::Unit::TestCase

  def setup
    @datetime     = LineOfCredit::DateTimeMock
    @datetime.now = DateTime.new(2010,01,01)
    @account      = LineOfCredit::Account.new(1000, 0.35, @datetime)
  end

  def teardown
    @datetime = nil
    @account  = nil
  end

  def test_account_initial_state
    assert_equal 0, @account.balance
    assert_equal 0, @account.transactions.length
    assert_equal @datetime.now, @account.created
    assert_equal 1000, @account.limit
    assert_equal 0.35, @account.apr
  end

  def test_example_1
    @account.withdraw(500)
    assert_equal 500, @account.balance
    assert_equal 1, @account.transactions.length
    transaction = @account.transactions.first
    assert_equal 500, transaction.amount
    assert_equal 500, transaction.balance
    assert_equal @datetime.now, transaction.created
    assert_equal :withdraw, transaction.kind
    @datetime.now += 30
    @account.charge_interest
    assert_equal 2, @account.transactions.length
    transaction = @account.transactions.last
    assert_equal 14.38, transaction.amount
    assert_equal 514.38, transaction.balance
    assert_equal @datetime.now, transaction.created
    assert_equal :interest, transaction.kind
  end

  def test_example_2
    @account.withdraw(500)
    @datetime.now += 15
    @account.payment(200)
    @datetime.now += 10
    @account.withdraw(100)
    @datetime.now += 5
    @account.charge_interest
    assert_equal 4, @account.transactions.length
    transaction = @account.transactions.last
    assert_equal 11.99, transaction.amount
    assert_equal 411.99, transaction.balance
    assert_equal @datetime.now, transaction.created
    assert_equal :interest, transaction.kind
  end

  def test_interest_on_principle_only_1
    @account.withdraw(500)
    @datetime.now += 30
    @account.charge_interest
    @datetime.now += 30
    @account.charge_interest
    assert_equal 3, @account.transactions.length
    transaction = @account.transactions.last
    assert_equal 14.38, transaction.amount
    assert_equal 528.76, transaction.balance
    assert_equal @datetime.now, transaction.created
    assert_equal :interest, transaction.kind
  end

  def test_payment_deduct_from_interest_before_principle
    @account.withdraw(500)
    @datetime.now += 15
    @account.payment(200)
    @datetime.now += 10
    @account.withdraw(100)
    @datetime.now += 5
    @account.charge_interest
    @account.payment(111.99)
    transaction = @account.transactions.last
    assert_equal 111.99, transaction.amount
    assert_equal 300, transaction.principle
    assert_equal 0, transaction.interest
    assert_equal :payment, transaction.kind
  end

end

