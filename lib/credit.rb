require 'date'

module LineOfCredit

  class DateTimeMock

    class << self
      attr_accessor(:now)
    end

  end

  class Transaction

    attr_reader(:created, :principle, :interest, :amount, :kind)

    def initialize(created, principle, interest, amount, kind)
      @created   = created
      @principle = principle
      @interest  = interest
      @amount    = amount
      @kind      = kind
    end

    def balance
      return principle + interest
    end

  end

  class Account

    attr_reader(:created, :limit, :apr, :transactions)

    def initialize(limit, apr, datetime=DateTime)
      assert_positive_non_zero_numeric(limit, "limit")
      assert_positive_non_zero_numeric(apr, "apr")
      unless datetime.respond_to?(:now)
        raise ArgumentError.new("datetime missing :now")
      end
      @datetime     = datetime
      @created      = datetime.now
      @limit        = limit
      @apr          = apr
      @transactions = []
    end

    def balance
      if transactions.empty?
        return 0
      else
        return transactions.last.balance
      end
    end

    def payment(amount)
      assert_positive_non_zero_numeric(amount, "amount")
      p = principle
      i = interest
      if i > 0
        i -= amount
        if i < 0
          p += i
          i = 0
        end
      else
        p -= amount
      end
      transactions.push(Transaction.new(@datetime.now, p, i, amount, :payment))
    end

    def withdraw(amount)
      assert_positive_non_zero_numeric(amount, "amount")
      if amount > (limit - balance)
        raise ArgumentError.new("Insufficient Balance")
      end
      p = principle + amount
      i = interest
      transactions.push(Transaction.new(@datetime.now, p, i, amount, :withdraw))
    end

    def charge_interest
      time = @datetime.now
      from = charge_interest_from
      upto = transactions.size
      list = transactions.slice(from..upto)
      unless list.empty?
        if time > list.last.created
          list.push(Transaction.new(time, principle, interest, 0, :stop))
        end
        old_interest = interest
        new_interest = 0
        (1..list.size-1).each do |i|
          a = list[i-1]
          b = list[i]
          days = b.created - a.created
          new_interest += (a.principle * apr) / 365 * days
        end
        new_interest = new_interest.round(2)
        all_interest = old_interest + new_interest
        transactions.push(Transaction.new(time, principle, all_interest, new_interest, :interest))
      end
    end

    private

    def principle
      if transactions.empty?
        return 0
      else
        return transactions.last.principle
      end
    end

    def interest
      if transactions.empty?
        return 0
      else
        return transactions.last.interest
      end
    end

    def charge_interest_from
      last = 0
      transactions.each_index do |i|
        if transactions[i].kind == :interest
          last = i
        end
      end
      return last
    end

    def assert_positive_non_zero_numeric(value, name)
      unless value.is_a?(Numeric)
        raise ArgumentError.new("#{name} should be Numeric")
      end
      unless value > 0
        raise ArgumentError.new("#{name} should be greater than zero")
      end
    end

  end

end
